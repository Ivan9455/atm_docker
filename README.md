# atm
* ```docker-compose up --build -d``` (запустить проект)
* ```docker exec -it readium_php_1 composer update``` (обновить composer)
* ```docker exec -it readium_php_1 php ./init``` (для выбора dev или prod)
* ```docker exec -it readium_php_1 php yii migrate --migrationPath=@yii/rbac/migrations``` (миграции rbac)
* ```docker exec -it readium_php_1 php yii migrate``` (запуск миграций)
docker-compose up --build -d
docker-compose exec php composer update
docker-compose exec php ./init
docker-compose exec php yii migrate --migrationPath=@yii/rbac/migrations
docker-compose exec php yii migrate
docker-compose exec php yii fixture

docker-compose stop
sudo rm -r docker/db/

#Info
Для того чтобы запустить проект для frontend,backend,otchet
1) frontend/config/main-local.php  если файл пустой  тогда добавить
```php
<?php
return [];

```
2) common/components/events/behavior/EventCheckStatusAccount.php там падает поэтому коментируем
```php
    public function checkStatusAccount()
    {
//        if (Utility::getCurrentUser()->employment_type_id === UserEmploymentType::FIRED) {
//            Yii::$app->user->logout();
//            if ($this->isApiAnyAction) {
//                echo 'Ваша учетная запись была заблокирована';
//                exit;
//            }
//
//            $mes = 'Ваша учетная запись была заблокирована';
//            Yii::$app->getSession()->setFlash('danger', $mes);
//            return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
//        }
        return false;
    }
```

3. common/config/main-local.php ,otchet/config/main-local.php
заменить 
```
        'db'     => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'mysql:host=localhost;dbname=atm72local',
            'username' => 'root',
            'password' => '',
            'charset'  => 'utf8mb4',
        ],
```
на код ниже

```
        'db'     => [
            'class'    => 'yii\db\Connection',
            'dsn' => 'mysql:host=db;dbname=' . getenv('MYSQL_DATABASE'),
            'username' => getenv('MYSQL_USER'),
            'password' => getenv('MYSQL_PASSWORD'),
            'charset'  => 'utf8mb4',
        ],
```
4. Была проблема при авторизации `otchet` решение:
otchet/config/main-local.php 
там поменять domain (session,user) на текущий который есть локально у меня 
если этого не сделать тогда пользовательне авторизуеться (проблема в сессии)

класс для работы с датами
common\components\Utility::getDateNow()

Поиск и удаление веток по шаблону

git branch | grep hotfix | xargs git branch -D

#scp example 

scp atm@192.168.2.2:/home/atm/data/dev1/neo.sd.atm72.ru/api/runtime/reqdocs/2021/02/04/_5576318_J173769.docx /home/ivan/PhpstormProjects/neo.sd.atm72.ru/server_dock/

# run test (unit)
php vendor/codeception/codeception/codecept -c codeception.yml run


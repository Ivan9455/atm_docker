phpc:
	docker-compose exec php bash

dbc:
	docker-compose exec db bash

start:
	docker-compose start

fixture:
	docker-compose exec php yii fixture "*"

stop:
	docker-compose stop

build:
	docker-compose up --build -d



https://stackoverflow.com/questions/34115174/error-related-to-only-full-group-by-when-executing-a-query-in-mysql
set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

create table requests_stats
(
    id                 int unsigned auto_increment
        primary key,
    contracts_group_id int(11) unsigned null,
    standart_light     json             null,
    standart           json             null,
    vip                json             null,
    premium_light      json             null,
    premium            json             null,
    signature          json             null,
    created_at         int(11) unsigned not null,
    updated_at         int(11) unsigned not null
);

create
    definer = root@`%` function GetTB(GivenID int) returns varchar(1024)
BEGIN
  DECLARE rv VARCHAR(1024);
  DECLARE ch INT;
  DECLARE currenttype VARCHAR(255);

  SET rv = null;
  SET currenttype = '';
  SET ch = GivenID;
  childloop: WHILE ch > 0 DO
    SELECT type INTO currenttype FROM
                                      (SELECT type FROM departments WHERE id = ch) A;

    IF currenttype = 'TB' THEN
      SET rv = ch;
      LEAVE childloop;
    END IF;

    SELECT IFNULL(parent_id,-1) INTO ch FROM
                                             (SELECT parent_id,type FROM departments WHERE id = ch) A;

  END WHILE;
  RETURN rv;
END;

create
    definer = root@`%` function GetGosb(GivenID int) returns varchar(1024)
BEGIN
    DECLARE rv VARCHAR(1024);
    DECLARE ch INT;
    DECLARE currenttype VARCHAR(255);

    SET rv = '';
    SET currenttype = '';
    SET ch = GivenID;
    
    childloop: WHILE ch > 0 DO
      SELECT type INTO currenttype FROM
        (SELECT type FROM departments WHERE id = ch) A;

      IF currenttype = 'gosb' THEN
        SET rv = ch;
        LEAVE childloop;
      END IF;
      
      SELECT IFNULL(parent_id,-1) INTO ch FROM
       (SELECT parent_id,type FROM departments WHERE id = ch) A;

    END WHILE;
    RETURN rv;
  END;
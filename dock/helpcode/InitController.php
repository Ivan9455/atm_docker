<?php

namespace console\controllers;

use api\models\User;

use common\components\request\act_generator\generator\EastBankActGenerator;
use common\models\Request;
use yii\console\Controller;

use Yii;

class InitController extends Controller
{
    public function actionIndex()
    {
        $sql = "set global sql_mode ='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
                set session sql_mode ='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';";
        Yii::$app->db->createCommand(sprintf(
            'ALTER DATABASE %s COLLATE utf8mb4_general_ci',
            getenv('MYSQL_DATABASE')
        ))->execute();
        Yii::$app->db->createCommand($sql)->execute();
    }

    public function actionCloseRequest()
    {
        $request = Request::findOne(5034199);
        $request->executed();
        if (!$request->save()) {
            var_dump($request->getFirstErrors());
        }
        //$ebag = new EastBankActGenerator($request);
        //$ebag->generate();
        die;
    }

    public function actionUserToken()
    {
        $user = User::findOne(4145);
        $user->setPassword('QualityOfService12');
        var_dump(sprintf(
            '%s::%s',
            $user->email,
            $user->password
        ));
        die;
    }

    public function actionEastTest()
    {
        $request = Request::findOne(5036087);
        $eastBankActGenerator = new EastBankActGenerator($request);
        var_dump($eastBankActGenerator->generate()->image);
        die;
        //var_dump($request->id);die;

    }
}

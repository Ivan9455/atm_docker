
- docker-php-ext-configure: Эта команда позволяет вам предоставить настраиваемые аргументы для расширения.
- docker-php-ext-install: Используйте эту команду для установки новых расширений в ваш контейнер.
- docker-php-ext-enable: Эта команда может использоваться для включения расширений PHP.

        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
